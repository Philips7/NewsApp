import { NTPReloadPage } from './app.po';

describe('ntpreload App', () => {
  let page: NTPReloadPage;

  beforeEach(() => {
    page = new NTPReloadPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
