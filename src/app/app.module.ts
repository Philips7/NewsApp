import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import 'hammerjs';
import { AppComponent } from './app.component';
import {DataService} from "./service/data.service";
import { NavbarComponent } from './component/navbar/navbar.component';
import { FooterComponent } from './component/footer/footer.component';
import { SourcesComponent } from './component/sources/sources.component';
import { UniquePipePipe } from './pipe/unique-pipe.pipe';
import { ArticleComponent } from './component/article/article.component';
import {RouterModule} from "@angular/router";
import { LoginComponent } from './component/login/login.component';
import {AngularFireModule, AuthMethods, AuthProviders} from "angularfire2";
import {UserService} from "./service/user.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  MdButtonModule, MdCardModule, MdDialogModule, MdInputModule, MdMenuModule, MdOptionModule,
  MdSelectModule, MdTabsModule, MdSidenavModule
} from "@angular/material";
import {ShareButtonsModule} from "ng2-sharebuttons";
import { LogoutComponent } from './component/logout/logout.component';
import { RegisterComponent } from './component/register/register.component';
import {AuthGuard} from "./guards/auth.guard";
import { MySourcesComponent } from './component/my-sources/my-sources.component';
import { MyCategoriesComponent } from './component/my-categories/my-categories.component';
import { CategoryListComponent } from './component/category-list/category-list.component';

const config = {
  apiKey: "AIzaSyDKmuisuVwqYYXCdRnKkVybahjimVV5iPc",
  authDomain: "newsfilter-5a357.firebaseapp.com",
  databaseURL: "https://newsfilter-5a357.firebaseio.com",
  projectId: "newsfilter-5a357",
  storageBucket: "",
  messagingSenderId: "190426733641"
};

const auth = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    SourcesComponent,
    UniquePipePipe,
    ArticleComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    MySourcesComponent,
    MyCategoriesComponent,
    CategoryListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ShareButtonsModule,
    MdTabsModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdSelectModule,
    MdOptionModule,
    MdMenuModule,
    MdCardModule,
    MdDialogModule,
    MdSidenavModule,
    MdButtonModule,
    RouterModule.forRoot([
      {path: 'sources', component: SourcesComponent},
      {path: 'articles/:name', component: ArticleComponent},
      {path: 'mySources', component: MySourcesComponent, canActivate: [AuthGuard]},
      {path: 'myCategories', component: MyCategoriesComponent, canActivate: [AuthGuard]},
      {path: 'login', component: LoginComponent},
      {path: 'logout', component: LogoutComponent},
      {path: 'register', component: RegisterComponent},
      {path: '', redirectTo: 'sources', pathMatch: 'full'},
      {path: '*', redirectTo: 'sources', pathMatch: 'full'}
    ]),
    AngularFireModule.initializeApp(config, auth)
  ],
  providers: [
    DataService,
    UserService,
    AuthGuard
  ],
  entryComponents: [
    CategoryListComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
