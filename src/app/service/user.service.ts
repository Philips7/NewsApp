import {Injectable} from '@angular/core';
import {Subject} from "rxjs/Subject";
import {AngularFire, AuthMethods, AuthProviders} from "angularfire2";
import {Router} from "@angular/router";

@Injectable()
export class UserService {

  user: string;
  userChange: Subject<string> = new Subject<string>();
  error: Subject<Error> = new Subject<Error>();

  constructor(public af: AngularFire, private router: Router) {
    this.af.auth.subscribe(auth => {
      if (auth) {
        this.user = auth.auth.email;
        this.userChange.next(auth.auth.uid);
        localStorage.setItem('logged', auth.auth.uid);
      }
    });
  }

  login(email, password) {
    this.af.auth.login({
      email: email,
      password: password
    }, {provider: AuthProviders.Password, method: AuthMethods.Password})
      .catch(error => {
        this.error.next(error);
      }).then(response => {
      this.router.navigateByUrl("/myCategories");
    } );
  }

  register(email, password){
    this.af.auth.createUser({
      email: email,
      password: password
    }).catch(error => {
      this.error.next(error);
    }).then(response =>{
      this.router.navigateByUrl("/myCategories");
    });
  }

  logout() {
    this.af.auth.logout();
    localStorage.removeItem('logged');
  }

}
