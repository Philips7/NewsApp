import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {AngularFire, AngularFireDatabase, FirebaseListObservable} from "angularfire2";

@Injectable()
export class DataService {
  private _sourcesUrl: string = "https://newsapi.org/v1/sources?language=en";
  private _categoryUrl: string = "https://newsapi.org/v1/sources?category=";
  private _sourceUrl: string = "https://newsapi.org/v1/articles?apiKey=5bc7626fccc24e158f1f68c09e2c2ce7&source=";
  private _keyApi: string = "&apiKey=5bc7626fccc24e158f1f68c09e2c2ce7"

  database: any;
  sources: FirebaseListObservable<any>;
  categories: FirebaseListObservable<any>;
  userId: string;

  private response: any;
  public categoryList = [ 'business', 'entertainment', 'gaming', 'general', 'music',
    'politics', 'science-and-nature', 'sport', 'technology'];



  constructor(private _http: Http,
              private db: AngularFireDatabase) {
    this.userId = localStorage.getItem('logged');
  }

  getSources(): Observable<any> {
    return this._http.get(this._sourcesUrl)
      .map((response: any) => response.json())
      .do(data => console.log('All' + JSON.stringify(data)))
      .catch(this.handelError);
  }

  getCategory(category: string): Observable<any> {
    return this._http.get(this._categoryUrl + category)
      .map((response: any) => response.json())
      .do(data => console.log('All' + JSON.stringify(data)))
      .catch(this.handelError);
  }

  getSource(source: string): Observable<any> {
    let url = this._sourceUrl + source;
    return this._http.get(url)
      .map((response: any) => response.json())
      .do(data => console.log('All' + JSON.stringify(data)))
      .catch(this.handelError);
  }

  addCategoryFB(category) {
    this.userId = localStorage.getItem('logged');
    this.database = this.db.list(this.userId + '/categories/' + category);
    this.database.push(category);
  }

  getCategoriesFB() { //aby odczytać trzeba zrobić subscribe
    this.userId = localStorage.getItem('logged');
    return this.database = this.db.list(this.userId + '/categories/');
  }

  deleteCategory(category) {
    this.userId = localStorage.getItem('logged');
    this.database = this.db.list(this.userId + '/categories/');
    this.database.remove(category);
  }


  private handelError(error: Error) {
    console.error(error);
    return Observable.throw(error.message.toLowerCase() || 'Server error');
  }
}

