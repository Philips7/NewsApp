import {Component, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {DataService} from "../../service/data.service";
import {MdDialog} from "@angular/material";
import {CategoryListComponent} from "../category-list/category-list.component";
import {ICategories} from "../../interfaces/categories";
import {$} from "protractor";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-my-categories',
  templateUrl: './my-categories.component.html',
  styleUrls: ['./my-categories.component.css']
})
export class MyCategoriesComponent implements OnInit {

  private categories;
  private usersCategories: any[] = [];
  private sources: any = null;
  private error: any;

  constructor(private dataService: DataService, private dialog: MdDialog, private _router: ActivatedRoute) {
  }


  ngOnInit() {
    this.categories = this.dataService.getCategoriesFB().subscribe(items => {
      items.forEach(item => {
        console.log('Item:', item);
        if (this.usersCategories.indexOf(item.$key) == -1) {
          this.usersCategories.push(item.$key);
        }
        console.log("key: " + this.usersCategories);
      });
    });

  }

  addCategory(item) {
    this.dataService.addCategoryFB(item);
  }

  getCategory() {
    // this.usersCategories = this.dataService.getCategoriesFB().subscribe();
    // console.log("User Categories" + this.usersCategories);
  }

  getSource(source: string) {
    this.dataService.getCategory(source)
      .subscribe(sources => this.sources = sources,
        error2 => this.error = <any>error2);
  }

  openDialog() {

    this.dialog.open(CategoryListComponent);
  }

}

