import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthGuard} from "../../guards/auth.guard";
import {MdDialog} from "@angular/material";
import {LoginComponent} from "../login/login.component";
import {LogoutComponent} from "../logout/logout.component";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  routeLinks:any[];
  activeLinkIndex = 0;
  constructor(private router: Router, private authGuard: AuthGuard,private dialog: MdDialog) {
    this.routeLinks = [
      {label: 'Sources', link: '/sources', logged: true, notLogged: true},
      {label: 'Login', link: '/login', logged: true, notLogged: false},
      // {label: 'My Sources', link: '/mySources', logged: false, notLogged: true},
      {label: 'MyCategories', link: '/myCategories', logged: false, notLogged: true},
      {label: 'Register', link: '/register', logged: true, notLogged: false}];
  }

  openDialog() {
    this.dialog.open(LogoutComponent);
  }

  ngOnInit() {
  }

//

}
