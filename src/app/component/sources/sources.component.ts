import { Component, OnInit } from '@angular/core';
import {DataService} from "../../service/data.service";
import {MdDialog} from "@angular/material";
import {CategoryListComponent} from "../category-list/category-list.component";
import {AuthGuard} from "../../guards/auth.guard";


@Component({
  selector: 'app-sources',
  templateUrl: './sources.component.html',
  styleUrls: ['./sources.component.css']
})
export class SourcesComponent implements OnInit {

  private sources: any;
  private categorySources: any;
  private error: any;
  private categories = [ 'business', 'entertainment', 'gaming', 'general', 'music',
    'politics', 'science-and-nature', 'sport', 'technology'];

  foods = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor(private dataService: DataService, private dialog: MdDialog, authGuard: AuthGuard) { }

  ngOnInit() {
    this.dataService.getSources()
      .subscribe(sources => this.sources = sources,
          error2 => this.error = <any>error2);
  }

  openDialog(){
    this.dialog.open(CategoryListComponent);
  }


  getCategories(category: string) {
    this.dataService.getCategory(category)
        .subscribe(sources => this.sources = sources,
          error2 => this.error = <any>error2);
  }

  getSource(source: string) {
    this.dataService.getSource(source)
      .subscribe(sources => this.sources = sources,
        error2 => this.error = <any>error2);
  }

  getArticle(article: string){
    this.dataService.getSource(article)
      .subscribe(sources => this.sources = sources,
        error2 => this.error = <any>error2);
  }

  addCategory(item) {
    this.dataService.addCategoryFB(item);
  }

}
