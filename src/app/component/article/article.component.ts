import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DataService} from "../../service/data.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  private articles: any;
  private error: any;

  constructor(private _router: ActivatedRoute, private _data: DataService) { }

  ngOnInit() {
    let name = this._router.snapshot.params['name'];
    console.log(name);

    this._data.getSource(name)
      .subscribe(sources => this.articles = sources,
        error2 => this.error = <any>error2);
  }


}
