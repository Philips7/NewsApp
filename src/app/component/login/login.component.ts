import { Component, OnInit } from '@angular/core';
import {UserService} from "../../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  name: string;
  password: string;
  error: Error;
  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.userService.error.subscribe(error => this.error = error);
  }

  login() {
    this.userService.login(this.name,  this.password);
  }

}
