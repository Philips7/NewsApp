import { Component, OnInit } from '@angular/core';
import {DataService} from "../../service/data.service";

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  private categoryList: string[];


  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.categoryList = this.dataService.categoryList;
  }

  addCategory(item: string){
    this.dataService.addCategoryFB(item);
    console.log("Dodano: " + item)
  }
}
