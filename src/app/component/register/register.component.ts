import { Component, OnInit } from '@angular/core';
import {UserService} from "../../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: string;
  password: string;
  error: Error;


  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.error.subscribe(error => this.error = error);
  }

  register(){
     this.userService.register(this.name, this.password);
     if(localStorage.getItem("logged") != null) {
       this.router.navigateByUrl("/sources");
     }
  }
}
